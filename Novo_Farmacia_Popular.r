library(data.table)
library(plyr)
library(dplyr)
library(tidyr)




# houveram várias alterações durante a programação. Neste aquivo eu irei gerar os novos dados

gc()

# Diretório do projeto:
dir.fm2 <- "\\\\sfs\\Bases\\Farmacia_Popular\\Exportacao"

#### II. RL_SOLICITACAO_MEDICAMENTO: AGREGAR POR CO_CODIGO_BARRA E MONTH(DT_VENDA):  ####
SOLICITACAO_MEDICAMENTO <- fread(file.path(dir.fm2, "RL_SOLICITACAO_MEDICAMENTO.csv"), 
                                 select = c("CO_SOLICITACAO", "CO_CODIGO_BARRA", "DT_VENDA", "QT_PRESCRITA", 
                                            "QT_AUTORIZADA", "QT_DEVOLVIDA", "VL_PRECO_VENDA", "VL_PRECO_SUBSIDIADO"), 
                                 sep = "auto", sep2 = "auto", header = T)

Sys.sleep(120)
gc()
SOLICITACAO_MEDICAMENTO <- data.table(SOLICITACAO_MEDICAMENTO)
gc()
gc()
Sys.sleep((120))
# substituir NA por zero
SOLICITACAO_MEDICAMENTO[is.na(SOLICITACAO_MEDICAMENTO)] <- 0
Sys.sleep((300))
gc()

## Adicionando as UF's
# FAZER JOIN DA DA TB_SOLICITACAO COM A TB_FARMACIA PELO NU_CNPJ E, POR FIM, DA TB_FARMACIA COM A TABELA DE MUNICIPIOS
# DO IBGE PELO CO_MUNICIPIO_IBGE.
# OBS: "puxar" CO_UNID_CONCENTRACAO de TB_CONCENTRACAO

timestamp("Início")
dir.fm2 <- "\\\\sfs\\Bases\\Farmacia_Popular\\Exportacao"
TB_SOLICITACAO <- fread(file.path(dir.fm2, "TB_SOLICITACAO.csv"), 
                                 select = c("CO_SOLICITACAO", "NU_CNPJ"), 
                                 sep = "auto", sep2 = "auto", header = T)
TB_SOLICITACAO <- data.table(TB_SOLICITACAO)

TB_FARMACIA <- fread(file.path(dir.fm2, "TB_FARMACIA.csv"), 
                        select = c("NU_CNPJ", "CO_MUNICIPIO_IBGE"), 
                        sep = "auto", sep2 = "auto", header = T)
TB_FARMACIA <- data.table(TB_FARMACIA)

ADD_UF <- left_join(TB_SOLICITACAO, TB_FARMACIA, by = "NU_CNPJ")
ADD_UF <- data.table(ADD_UF)
gc()
saveRDS(ADD_UF, "ADD_UF.rds")
rm(TB_SOLICITACAO,TB_FARMACIA)

#######################
# eu preciso das variáveis vl_sub E vl_ref que estao em Alteracoes_Vl_FarmaPop, para junta-la a SOLICITACAO_MEDICAMENTO
# eu preciso do CO_CATMAT:
# LIMPANDO RL_MEDICAMENTO_CONCENTRACAO
RL_MEDICAMENTO_CONCENTRACAO <- fread("Z:\\Farmacia_Popular\\Exportacao\\RL_MEDICAMENTO_CONCENTRACAO.csv")
TB_MEDICAMENTO_PHHC <- fread("C:\\Users\\guilherme.carvalho\\Documents\\TB_MEDICAMENTO_PHHC.csv")
TB_PRINCIPIO_ATIVO_PHHC <- fread("C:\\Users\\guilherme.carvalho\\Documents\\TB_PRINCIPIO_ATIVO_PHHC.csv")

# alguns nomes estão presentes tanto em RL_MEDICAMENTO_CONCENTRACAO, quanto em TB_MEDICAMENTO_PHHC
# vou remover os de RL_MEDICAMENTO_CONCENTRACAO

RL_MEDICAMENTO_CONCENTRACAO <- data.table(RL_MEDICAMENTO_CONCENTRACAO)

RL_MEDICAMENTO_CONCENTRACAO$DT_ALTERACAO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$DT_INCLUSAO <- NULL

RL_MEDICAMENTO_CONCENTRACAO <- full_join(RL_MEDICAMENTO_CONCENTRACAO,TB_MEDICAMENTO_PHHC, by = "CO_CODIGO_BARRA")

TB_PRINCIPIO_ATIVO_PHHC$DT_ALTERACAO <- NULL
TB_PRINCIPIO_ATIVO_PHHC$DT_INCLUSAO <- NULL
TB_PRINCIPIO_ATIVO_PHHC$dummy_PHHC <- NULL
RL_MEDICAMENTO_CONCENTRACAO$dummy_PHHC <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_ATIVO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_UNIDADE_PROPRIA <- NULL

RL_MEDICAMENTO_CONCENTRACAO <- left_join(RL_MEDICAMENTO_CONCENTRACAO, TB_PRINCIPIO_ATIVO_PHHC, by = "CO_PRINCIPIO_ATIVO")
RL_MEDICAMENTO_CONCENTRACAO <- data.table(RL_MEDICAMENTO_CONCENTRACAO)

# limpando base
names(RL_MEDICAMENTO_CONCENTRACAO)
RL_MEDICAMENTO_CONCENTRACAO$ST_PRINCIPIO_ATIVO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_UNIDADE_PROPRIA <- NULL
RL_MEDICAMENTO_CONCENTRACAO$QT_DIA_BLOQUEIO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$NU_REGISTRO_ANVISA <- NULL
RL_MEDICAMENTO_CONCENTRACAO$TP_MEDICAMENTO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$NO_MEDICAMENTO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$NO_FABRICANTE <- NULL
RL_MEDICAMENTO_CONCENTRACAO$DT_PRAZO_VALIDADE <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_COMERCIALIZACAO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$CO_USUARIO_INCLUSAO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$DS_APRESENTACAO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_UNIDADE_AQUI_TEM <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_UNIDADE_PROPRIA <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_MEDICAMENTO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$NU_CNPJ_FABRICANTE <- NULL
RL_MEDICAMENTO_CONCENTRACAO$QT_DIA_VALIDADE_RECEITA <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_ATIVO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$QT_MAXIMA <- NULL
RL_MEDICAMENTO_CONCENTRACAO$QT_USUAL <- NULL

rm(TB_MEDICAMENTO_PHHC, TB_PRINCIPIO_ATIVO_PHHC)

Alteracoes_Vl_Periodo <- fread("C:\\Users\\guilherme.carvalho\\Documents\\Alteracoes_Vl_Periodo.csv")
Alteracoes_Vl_Periodo$DT_ALTERACAO <- NULL
Alteracoes_Vl_Periodo$MES <- NULL
Alteracoes_Vl_Periodo$MES_ANO <- NULL


# montar base para juntar à SOLICITACAO_MEDICAMENTO
RL_MEDICAMENTO_CONCENTRACAO$DT_ALTERACAO <- NULL
RL_MEDICAMENTO_CONCENTRACAO$ST_TESTE <- NULL
RL_MEDICAMENTO_CONCENTRACAO$DT_INCLUSAO <- NULL

Alteracoes_Vl_Periodo <- rename(Alteracoes_Vl_Periodo, CO_CATMAT = CATMAT)
Alteracoes_Vl_Periodo$CO_CATMAT <- as.character(Alteracoes_Vl_Periodo$CO_CATMAT)
RL_MEDICAMENTO_CONCENTRACAO$CO_CATMAT <- substr(RL_MEDICAMENTO_CONCENTRACAO$CO_CATMAT, 4, 9)

# Fralda não tem CATMAT, como todas estão descritas como "FRALDA GERIÁTRICA TIPO 1", irei atribuir um CATMAT para juntar
Alteracoes_Vl_Periodo$CO_CATMAT <- ifelse(is.na(Alteracoes_Vl_Periodo$CO_CATMAT), 123456, Alteracoes_Vl_Periodo$CO_CATMAT)
RL_MEDICAMENTO_CONCENTRACAO$CO_CATMAT <- ifelse(RL_MEDICAMENTO_CONCENTRACAO$DS_PRINCIPIO_ATIVO == "FRALDA GERIÁTRICA TIPO 1",
                                                123456, RL_MEDICAMENTO_CONCENTRACAO$CO_CATMAT)

RL_MEDICAMENTO_CONCENTRACAO <- unique(RL_MEDICAMENTO_CONCENTRACAO)

juntar <- left_join(RL_MEDICAMENTO_CONCENTRACAO, Alteracoes_Vl_Periodo, by = "CO_CATMAT")

## adicionando "juntar" (9456 linhas) à SOLICITACAO_MEDICAMENTO (1303492892 linhas)
gc()

juntar <- data.table(juntar)
names(SOLICITACAO_MEDICAMENTO)
names(juntar)

# inicio
gc()
timestamp()
SOLICITACAO_MEDICAMENTO <- left_join(SOLICITACAO_MEDICAMENTO, juntar, by = "CO_CODIGO_BARRA")
# fim
timestamp()
gc()

getwd()

SOLICITACAO_MEDICAMENTO <- data.table(SOLICITACAO_MEDICAMENTO)

# inicio
timestamp()
saveRDS(SOLICITACAO_MEDICAMENTO, "SOL_MED_DEMANDA2.rds")
# fim
timestamp()
gc()

# inicio
timestamp()
rm(SOLICITACAO_MEDICAMENTO)
gc()
SOL_MED_DEMANDA2 <- data.table(readRDS("SOL_MED_DEMANDA2.rds"))
# fim
timestamp()
gc()


# names(SOL_MED_DEMANDA2)
# SOL_MED_DEMANDA2$CO_CATMAT <- NULL

SOL_MED_DEMANDA2$CHECK_QT_UF_SUB <- SOL_MED_DEMANDA2$VL_PRECO_SUBSIDIADO/SOL_MED_DEMANDA2$VL_SUB
SOL_MED_DEMANDA2$CHECK_QT_UF_REF <- SOL_MED_DEMANDA2$VL_PRECO_VENDA/SOL_MED_DEMANDA2$VL_REF

##################################

gc()
SOL_MED_DEMANDA2 <- left_join(SOL_MED_DEMANDA2, ADD_UF, by = "CO_SOLICITACAO")
gc()
timestamp("Fim")

rm(ADD_UF)

## adicionando os nomes de UF
uf_municipios <- fread("uf_municipios.csv", sep = "auto", sep2 = "auto", header = T)
uf_municipios <- rename(uf_municipios, CO_MUNICIPIO_IBGE = CD_GCMUN)
# o codigo de mun de SOL_MED_DEMANDA2 possui 6 digitos, o de uf_municipios possui 7, vou deixa-lo com 6 dig.
uf_municipios$CO_MUNICIPIO_IBGE <- substr(uf_municipios$CO_MUNICIPIO_IBGE, 1, 6)
uf_municipios$CO_MUNICIPIO_IBGE <- as.integer(uf_municipios$CO_MUNICIPIO_IBGE)

timestamp("Início")
SOL_MED_DEMANDA2 <- left_join(SOL_MED_DEMANDA2, uf_municipios, by = "CO_MUNICIPIO_IBGE")
SOL_MED_DEMANDA2 <- data.table(SOL_MED_DEMANDA2)
timestamp("FIM")
gc()

# "puxando" CO_UNID_CONCENTRACAO de TB_CONCENTRACAO
TB_CONCENTRACAO <- fread(file.path(dir.fm2, "TB_CONCENTRACAO.csv"), 
                        select = c("CO_PRINCIPIO_ATIVO", "CO_UNID_CONCENTRACAO"), 
                        sep = "auto", sep2 = "auto", header = T)
TB_CONCENTRACAO <- data.table(TB_CONCENTRACAO)

# juntando TB_CONCENTRACAO com SOL_MED_DEMANDA2 pelo CO_PRINCIPIO_ATIVO
# TB_CONCENTRACAO possui linhas repetidas, preciso remove-las:
TB_CONCENTRACAO <- unique(TB_CONCENTRACAO)


timestamp("Início")
SOL_MED_DEMANDA2 <- left_join(SOL_MED_DEMANDA2, TB_CONCENTRACAO, by = "CO_PRINCIPIO_ATIVO")
SOL_MED_DEMANDA2 <- data.table(SOL_MED_DEMANDA2)
timestamp("FIM")
gc()


# criando uma base de testes antes de utilizar a base principal
teste <- data.table(SOL_MED_DEMANDA2[1:10000])
summary(teste$VL_SUB)
# transformando em numérico e substituindo "," por "."
teste$VL_SUB <- as.numeric(gsub(",",".",teste$VL_SUB))
teste$VL_REF <- as.numeric(gsub(",",".",teste$VL_REF))

teste$CHECK_QT_UF_SUB <- teste$VL_PRECO_SUBSIDIADO/teste$VL_SUB
teste$CHECK_QT_UF_REF <- teste$VL_PRECO_VENDA/teste$VL_REF
teste$CHECK_ULTRAPASSOU_LIMITE <- ifelse(teste$QT_PRESCRITA > teste$VL_PRECO_SUBSIDIADO, 1, 0)

## Criar máximos e mínimos

## FALTA incluir PERIODO (já incluso em 21/08/2019) em group_by
gc()
timestamp("Início")
max_min <- teste %>%
  dplyr::group_by(NM_UF, CO_PRINCIPIO_ATIVO, QT_CONCENTRACAO, CO_UNID_CONCENTRACAO, QT_APRESENTACAO, CO_UNID_APRESENTACAO) %>%
  dplyr::summarise(VL_PRECO_VENDA_MAX=max(VL_PRECO_VENDA), VL_PRECO_VENDA_MIN=min(VL_PRECO_VENDA),
                       VL_PRECO_SUBSIDIADO_MAX=max(VL_PRECO_SUBSIDIADO), VL_PRECO_SUBSIDIADO_MIN=min(VL_PRECO_SUBSIDIADO),
                       PRECO_VENDA_UNITARIO_MAX=max(preco_venda_unitario), PRECO_VENDA_UNITARIO_MIN=min(preco_venda_unitario),
                       PRECO_SUBS_UNITARIO_MAX=max(preco_subs_unitario), PRECO_SUBS_UNITARIO_MIN=min(preco_subs_unitario))
Sys.sleep(120)
gc()
timestamp("FIM")

saveRDS(max_min, "max_min.rds")



################ criando periodo

Alteracoes_Vl_FarmaPop <- fread("C:\\Users\\guilherme.carvalho\\Documents\\Alteracoes_Vl_FarmaPop.csv")
Alteracoes_Vl_FarmaPop$ANO <- substr(Alteracoes_Vl_FarmaPop$DT_ALTERACAO, 7, 10)

Alteracoes_Vl_FarmaPop$MES <- substr(Alteracoes_Vl_FarmaPop$DT_ALTERACAO, 4, 5)
Alteracoes_Vl_FarmaPop$MES <- as.integer(Alteracoes_Vl_FarmaPop$MES)

Alteracoes_Vl_FarmaPop$MES_ANO <- substr(Alteracoes_Vl_FarmaPop$DT_ALTERACAO, 4, 10)

Alteracoes_Vl_FarmaPop$UF <- NULL
Alteracoes_Vl_FarmaPop$TAMANHO <- NULL
Alteracoes_Vl_FarmaPop$MEDICAMENTO <- NULL
Alteracoes_Vl_FarmaPop$INDICACAO <- NULL

Alteracoes_Vl_FarmaPop$P1 <- ifelse(Alteracoes_Vl_FarmaPop$MES == 2, "Janeiro",
                                    ifelse(Alteracoes_Vl_FarmaPop$MES == 3, "Fevereiro",
                                           ifelse(Alteracoes_Vl_FarmaPop$MES == 4, "Marco",
                                                  ifelse(Alteracoes_Vl_FarmaPop$MES == 5, "Abril",
                                                         ifelse(Alteracoes_Vl_FarmaPop$MES == 6, "Maio",
                                                                ifelse(Alteracoes_Vl_FarmaPop$MES == 7, "Junho",
                                                                       ifelse(Alteracoes_Vl_FarmaPop$MES == 8, "Julho",
                                                                              ifelse(Alteracoes_Vl_FarmaPop$MES == 9, "Agosto",
                                                                                     ifelse(Alteracoes_Vl_FarmaPop$MES == 10, "Setembro",
                                                                                            ifelse(Alteracoes_Vl_FarmaPop$MES == 11, "Outubro",
                                                                                                   ifelse(Alteracoes_Vl_FarmaPop$MES == 12, "Novembro", "Dezembro"
                                                                                                          )))))))))))


Alteracoes_Vl_FarmaPop$P2 <- ifelse(Alteracoes_Vl_FarmaPop$MES == 2, "Fevereiro - Dezembro",
                                    ifelse(Alteracoes_Vl_FarmaPop$MES == 3, "Março - Dezembro",
                                           ifelse(Alteracoes_Vl_FarmaPop$MES == 4, "Abril - Dezembro",
                                                  ifelse(Alteracoes_Vl_FarmaPop$MES == 5, "Maio - Dezembro",
                                                         ifelse(Alteracoes_Vl_FarmaPop$MES == 6, "Junho - Dezembro",
                                                                ifelse(Alteracoes_Vl_FarmaPop$MES == 7, "Julho - Dezembro",
                                                                       ifelse(Alteracoes_Vl_FarmaPop$MES == 8, "Agosto - Dezembro",
                                                                              ifelse(Alteracoes_Vl_FarmaPop$MES == 9, "Setembro - Dezembro",
                                                                                     ifelse(Alteracoes_Vl_FarmaPop$MES == 10, "Outubro - Dezembro",
                                                                                            ifelse(Alteracoes_Vl_FarmaPop$MES == 11, "Novembro - Dezembro", "Dezembro"
                                                                                                   ))))))))))


write.csv2(Alteracoes_Vl_FarmaPop, "Alteracoes_Vl_Periodo.csv", row.names = FALSE)
Alteracoes_Vl_Periodo <- fread("Alteracoes_Vl_Periodo.csv")

if(Alteracoes_Vl_FarmaPop$MES == 2) Alteracoes_Vl_FarmaPop$P1 = "teste"


Alteracoes_Vl_FarmaPop <- unique(Alteracoes_Vl_FarmaPop)



######### verificar se  posso apagar:
## criando uma base só com o CPF único e com os PORTEs de cada CPF ao longo dos contratos
# substitui P_ATIVO por ID
teste <- group_by(Alteracoes_Vl_FarmaPop, P_ATIVO, MES_ANO)


teste_t <- summarise(teste, DT_ALTERACAOz = unique(MES_ANO))
teste_t <- as.data.table(teste_t)
teste_t <- setorder(teste_t, MES_ANO)
# aqui a "mágica" acontece (usar mes e ano ao inves de DT)
teste_PROGRs <- spread(teste_t, MES_ANO, DT_ALTERACAOz)


## renomeando as variáveis 
for (i in 1:15){
  names(teste_PROGRs)[i+1] <- sprintf("Prog%d", i)
}

































